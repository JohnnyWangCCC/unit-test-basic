package com.afs.unittest;

import com.afs.unittest.Project.Project;
import com.afs.unittest.Project.ProjectType;
import com.afs.unittest.exception.UnexpectedProjectTypeException;
import com.afs.unittest.expense.ExpenseType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ExpenseServiceTest {
    @Test
    void should_return_internal_expense_type_when_getExpenseCodeByProject_given_internal_project() {
        // given
        Project project = new Project(ProjectType.INTERNAL, "project");
        ExpenseService expenseService = new ExpenseService();

        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(project);

        // then
        assertEquals(expenseCodeByProject, ExpenseType.INTERNAL_PROJECT_EXPENSE);
    }

    @Test
    void should_return_expense_type_A_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_A() {
        // given
        Project project = new Project(ProjectType.EXTERNAL, "Project A");
        ExpenseService expenseService = new ExpenseService();

        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(project);

        // then
        assertEquals(expenseCodeByProject, ExpenseType.EXPENSE_TYPE_A);
    }

    @Test
    void should_return_expense_type_B_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_B() {
        // given
        Project project = new Project(ProjectType.EXTERNAL, "Project B");
        ExpenseService expenseService = new ExpenseService();

        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(project);

        // then
        assertEquals(expenseCodeByProject, ExpenseType.EXPENSE_TYPE_B);
    }

    @Test
    void should_return_other_expense_type_when_getExpenseCodeByProject_given_project_is_external_and_has_other_name() {
        // given
        Project project = new Project(ProjectType.EXTERNAL, "Other Name");
        ExpenseService expenseService = new ExpenseService();

        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(project);

        // then
        assertEquals(expenseCodeByProject, ExpenseType.OTHER_EXPENSE);
    }

    @Test
    void should_throw_unexpected_project_exception_when_getExpenseCodeByProject_given_project_is_invalid() {
        // given
        Project project = new Project(null, "Other Name");
        ExpenseService expenseService = new ExpenseService();

        // when
        // then
        Assertions.assertThrows(UnexpectedProjectTypeException.class, ()-> expenseService.getExpenseCodeByProject(project));
    }
}